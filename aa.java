package EJERCICIO_JODER;

import static org.junit.Assert.*;

import java.util.Scanner;

import javax.xml.catalog.CatalogResolver;

import org.junit.Test;

public class aa {
	Scanner sc = new Scanner(System.in);
	@Test
	public void test() {
	
		assertEquals("El Gran MRajoy diu: \"tal que hola\"", ElTrabalenguasDeRajoy.solve(3, "hola que tal"));
		assertEquals("El Gran MRajoy diu: \"me gustan los catalanes hacen cosas\"", ElTrabalenguasDeRajoy.solve(2, "Los catalanes hacen cosas me gustan"));
		assertEquals("El Gran MRajoy diu: \"muchas tardes y buenas gracias\"", ElTrabalenguasDeRajoy.solve(5, "Buenas tardes y muchas gracias"));
		assertEquals("El Gran MRajoy diu: \"ustedes piensan antes de hablar o hablan tras pensar\"", ElTrabalenguasDeRajoy.solve(9, "Ustedes hablar antes de pensar o piensan tras hablan"));
		assertEquals("El Gran MRajoy diu: \"english is muy dificil\"", ElTrabalenguasDeRajoy.solve(4, "Muy dificil is english"));
		assertEquals("El Gran MRajoy diu: \"dije que iba a bajar impuestos y los estoy subiendo\"", ElTrabalenguasDeRajoy.solve(10, "Dije que iba a subiendo impuestos y los estoy bajar"));
		assertEquals("El Gran MRajoy diu: \"una frase con mas de dos palabras\"", ElTrabalenguasDeRajoy.solve(6, "palabras dos mas de con frase una"));
		assertEquals("El Gran MRajoy diu: \"si has fallado esta no le sabes\"", ElTrabalenguasDeRajoy.solve(7, "has fallado no sabes le si esta"));
		assertEquals("El Gran MRajoy diu: \"esto no es gracioso\"", ElTrabalenguasDeRajoy.solve(3, "gracioso no es esto"));
		assertEquals("El Gran MRajoy diu: \"bien hecho lo has conseguido\"", ElTrabalenguasDeRajoy.solve(5, "lo conseguido has hecho bien"));
	}

}
