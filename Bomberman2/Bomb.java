package Bomberman2;

public class Bomb {
	private Posicio pos;
	private int ticks;
	private int radius;
	private boolean hasExploded;

	public Bomb(int f, int c, int ticks, int radius) {
		this.pos = new Posicio(f, c);
		this.ticks = ticks;
		this.radius = radius;
		this.hasExploded = false;
	}

	public int getTicks() {
		return ticks;
	}

	public void setTicks(int secs) {
		this.ticks = secs;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public boolean hasExploded() {
		return hasExploded;
	}

	public void setExploded(boolean hasExploded) {
		this.hasExploded = hasExploded;
	}

	public Posicio getPos() {
		return pos;
	}
}
