package Bomberman2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import Core.Board;
import Core.Window;
/**
 * 
 * @author Eric Morales
 * 
 */
public class Bomberman2 {

	static ArrayList<Enemigo> enemies = new ArrayList<>();
	static ArrayList<Bomb> bombs = new ArrayList<>();
	static ArrayList<Explosion> explosions = new ArrayList<>();
	static ArrayList<Posicio> powerups = new ArrayList<>();
	static boolean end = false;

	static int bombMaxUp = 0;
	static int bombRadiusUp = 0;
	
	public static void main(String[] args) throws InterruptedException {
		Scanner sc = new Scanner(System.in);
		Board t = new Board();
		Window f = new Window(t);
		t.setActborder(false);
		t.setColorbackground(0xFFFFFF);
		int[][] matriz = { { 9, 9, 9, 9, 9, 9, 9 }, 
							{ 9, 3, 0, 2, 0, 0, 9 },
							{ 9, 0, 1, 0, 1, 0, 9 },
							{ 9, 2, 0, 2, 0, 2, 9 }, 
							{ 9, 0, 1, 0, 1, 0, 9 }, 
							{ 9, 0, 0, 2, 0, 0, 9 }, 
							{ 9, 9, 9, 9, 9, 9, 9 }

		};// mapa hardcodeado
		int[][] matrizBombas = new int[7][7];// matriz de bombas
		int[][] matrizPowerUps = new int[7][7];
		Posicio pbomberman = new Posicio(1, 1); // bomberman empieza en 1,1
		matriz[pbomberman.f][pbomberman.c] = 3;
		initEnemies(matriz);
		printMatriz(matriz);

		while (!end) {
			Thread.sleep(60);
			int maxBombs = 2 + bombMaxUp - bombs.size();
			Set<Character> instruccio = f.getKeysDown();
			pbomberman = moure(matriz, matrizBombas, pbomberman.f, pbomberman.c, instruccio);
			if (instruccio.contains('z') && maxBombs != 0) {
				addBomba(pbomberman, matrizBombas);
			}
			enemyAI(matriz, matrizBombas);
			startTicking();
			checkExploded(matriz, matrizBombas, matrizPowerUps);
			checkEnemies(matriz);
			tickExplosions(matrizBombas);
			checkEnemyCollision(pbomberman, matriz);
			obtainPowerUp(pbomberman, matrizPowerUps);
			checkVictory();
			printMatrizGrafica(matriz, matrizBombas, matrizPowerUps, t, f);
		}
	}
/**
 * Inicialitza els enemics
 * @param matriz
 */
	private static void initEnemies(int[][] matriz) {
		enemies.add(new Enemigo(5, 5, 2));
		enemies.add(new Enemigo(1, 5, 2));

		for (Enemigo enemy : enemies) {
			matriz[enemy.getPos().f][enemy.getPos().c] = 4;
		}

	}
	/**
	 * Mira si hasa guanyat
	 */
	private static void checkVictory() {
		if (enemies.isEmpty()) {
			System.out.println("HAS GANADO");
			end = true;
		}
	}
	/**
	 * Mou els enemics pel mapa
	 * @param matriz
	 * @param matrizBombas
	 */
	private static void enemyAI(int[][] matriz, int[][] matrizBombas) {
		Random rnd = new Random();
		char[] movements = { 'w', 'a', 's', 'd' };
		enemies.forEach(enemy -> {
			if (rnd.nextInt(0, 6) == 0) {
				Set<Character> movement = new HashSet<Character>();
				movement.add(movements[rnd.nextInt(0, 4)]);

				enemy.setPos(moure(matriz, matrizBombas, enemy.getPos().f, enemy.getPos().c, movement));
			}
		});
	}

	/**
	 * Fa el compte en rere per explotar
	 */
	private static void startTicking() {
		bombs.forEach(bomb -> {
			bomb.setTicks(bomb.getTicks() - 1);
			if (bomb.getTicks() == 0) {
				bomb.setExploded(true);
			}
		});
	}
/**
 * Serveix per posar bombes a la matriu
 * @param pbomberman
 * @param matrizBombas
 */
	private static void addBomba(Posicio pbomberman, int[][] matrizBombas) {
		Bomb bomb = new Bomb(pbomberman.f, pbomberman.c, 25, 1 + bombRadiusUp);
		bombs.add(bomb);
		matrizBombas[bomb.getPos().f][bomb.getPos().c] = 6;
	}
/**
 * Comproba si ha explotat la bomba
 * @param matriz
 * @param matrizBombas
 * @param matrizPowerUps
 */
	private static void checkExploded(int[][] matriz, int[][] matrizBombas, int[][] matrizPowerUps) {
		ArrayList<Bomb> explodedBombs = new ArrayList<>();
		bombs.forEach(bomb -> {
			if (bomb.hasExploded()) {
				explodedBombs.add(bomb);
				explode(bomb, matriz, matrizBombas, matrizPowerUps);
			}
		});
		bombs.removeAll(explodedBombs);
	}
/**
 * Explota la bomba
 * @param bomb
 * @param matriz
 * @param matrizBombas
 * @param matrizPowerUps
 */
	private static void explode(Bomb bomb, int[][] matriz, int[][] matrizBombas, int[][] matrizPowerUps) {
		System.out.println("BOOM");
		Posicio bpos = bomb.getPos();
		matrizBombas[bpos.f][bpos.c] = 5;
		for (int i = 0; i <= bomb.getRadius(); i++) {
			int newPos = bpos.f - i;
			if (!isOutOfBounds(newPos, bpos.c, matrizBombas)) {
				if (matriz[newPos][bpos.c] == 1 || matriz[newPos][bpos.c] == 9) {
					break;
				}
				checkCollision(new Posicio(newPos, bpos.c), matriz, matrizBombas, matrizPowerUps);
				explosions.add(new Explosion(newPos, bpos.c, 6));
				matrizBombas[newPos][bpos.c] = 5;
			}
		}
		for (int i = 0; i <= bomb.getRadius(); i++) {
			int newPos = bpos.f + i;
			if (!isOutOfBounds(newPos, bpos.c, matrizBombas)) {
				if (matriz[newPos][bpos.c] == 1 || matriz[newPos][bpos.c] == 9) {
					break;
				}
				checkCollision(new Posicio(newPos, bpos.c), matriz, matrizBombas, matrizPowerUps);
				explosions.add(new Explosion(newPos, bpos.c, 6));
				matrizBombas[newPos][bpos.c] = 5;
			}
		}
		for (int i = 0; i <= bomb.getRadius(); i++) {
			int newPos = bpos.c - i;
			if (!isOutOfBounds(bpos.f, newPos, matrizBombas)) {
				if (matriz[bpos.f][newPos] == 1 || matriz[newPos][bpos.c] == 9) {
					break;
				}
				checkCollision(new Posicio(bpos.f, newPos), matriz, matrizBombas, matrizPowerUps);
				explosions.add(new Explosion(bpos.f, newPos, 6));
				matrizBombas[bpos.f][newPos] = 5;
			}
		}
		for (int i = 0; i <= bomb.getRadius(); i++) {
			int newPos = bpos.c + i;
			if (!isOutOfBounds(bpos.f, newPos, matrizBombas)) {
				if (matriz[bpos.f][newPos] == 1 || matriz[newPos][bpos.c] == 9) {
					break;
				}
				checkCollision(new Posicio(bpos.f, newPos), matriz, matrizBombas, matrizPowerUps);
				explosions.add(new Explosion(bpos.f, newPos, 6));
				matrizBombas[bpos.f][newPos] = 5;
			}
		}
	}
/**
 * Mira amb que ha xocat l'explosio
 * @param p
 * @param matriz
 * @param matrizBombas
 * @param matrizPowerUps
 */
	private static void checkCollision(Posicio p, int[][] matriz, int[][] matrizBombas, int[][] matrizPowerUps) {
		if (matriz[p.f][p.c] == 2) {
			System.out.println("PARED");
			matriz[p.f][p.c] = 0;
			powerUpSpawner(p, matrizPowerUps);
		} else if (matrizBombas[p.f][p.c] == 6) {
			System.out.println("BOMBA");
			bombs.forEach(bomb -> {
				if (bomb.getPos().f == p.f && bomb.getPos().c == p.c) {
					bomb.setExploded(true);
				}
			});
		} else if (matriz[p.f][p.c] == 3) {
			System.out.println("HAS MUERTO");
			end = true;
			matriz[p.f][p.c] = 0;
		} else if (matriz[p.f][p.c] == 4) {
			enemies.forEach(enemy -> {
				if (enemy.getPos().f == p.f && enemy.getPos().c == p.c) {
					enemy.setHp(enemy.getHp() - 1);
					if (enemy.getHp() == 0) {
						enemy.setAlive(false);
					}
				}
			});
		}
	}
/**
 * Posa un Power Up a la matriu
 * @param pos
 * @param matrizPowerUps
 */
	private static void powerUpSpawner(Posicio pos, int[][] matrizPowerUps) {
		Random rnd = new Random();
		if (rnd.nextInt(5) == 0) {
			matrizPowerUps[pos.f][pos.c] = rnd.nextBoolean() ? 7 : 8;
			powerups.add(new Posicio(pos.f, pos.c));
		}
	}
/**
 * Serveix per agafar el power up amb el teu personatge
 * @param pbomberman
 * @param matrizPowerUps
 */
	private static void obtainPowerUp(Posicio pbomberman, int[][] matrizPowerUps) {
		ArrayList<Posicio> obtainedPowerUps = new ArrayList<>();
		powerups.forEach(powerup -> {
			if (pbomberman.f == powerup.f && pbomberman.c == powerup.c) {
				obtainedPowerUps.add(pbomberman);
				if (matrizPowerUps[powerup.f][powerup.c] == 7) {
					bombMaxUp=1;
				} else if (matrizPowerUps[powerup.f][powerup.c] == 8) {
					bombRadiusUp=1;
				}
				matrizPowerUps[powerup.f][powerup.c] = 0;
			}
		});
		powerups.removeAll(obtainedPowerUps);
	}
/**
 * Mira si t'has xocat amb un enemic
 * @param pbomberman
 * @param matriz
 */
	private static void checkEnemyCollision(Posicio pbomberman, int[][] matriz) {
		enemies.forEach(enemy -> {
			if (enemy.getPos().f == pbomberman.f && enemy.getPos().c == pbomberman.c) {
				System.out.println("HAS MUERTO");
				matriz[pbomberman.f][pbomberman.c] = 4;
				end = true;
			}
		});
	}
/**
 * Mira quant temps està la explosió
 * @param matrizBombas
 */
	private static void tickExplosions(int[][] matrizBombas) {
		ArrayList<Explosion> endedExplosions = new ArrayList<>();
		explosions.forEach(explosionTile -> {
			explosionTile.setTicks(explosionTile.getTicks() - 1);
			if (explosionTile.getTicks() == 0) {
				matrizBombas[explosionTile.getPos().f][explosionTile.getPos().c] = 0;
				endedExplosions.add(explosionTile);
			}
		});
		explosions.removeAll(endedExplosions);
	}
/**
 * Mira si els enemics segueixen vius
 * @param matriz
 */
	private static void checkEnemies(int[][] matriz) {
		ArrayList<Enemigo> deadEnemies = new ArrayList<>();
		enemies.forEach(enemy -> {
			if (!enemy.isAlive()) {
				deadEnemies.add(enemy);
				matriz[enemy.getPos().f][enemy.getPos().c] = 0;
			}
		});
		enemies.removeAll(deadEnemies);
	}
/**
 * Serveix per moure el personatge
 * @param matriz
 * @param matrizBombas
 * @param h
 * @param v
 * @param instruccio
 * @return
 */
	private static Posicio moure(int[][] matriz, int[][] matrizBombas, int h, int v, Set<Character> instruccio) {
		int pj = matriz[h][v];
		if (instruccio.contains('w')) {
			matriz[h][v] = 0;

			h--;
			if (isOutOfBounds(h, v, matriz) || matriz[h][v] == 1 ||  matriz[h][v] == 9 || matriz[h][v] == 2 || matrizBombas[h][v] != 0) {
				h++;
				matriz[h][v] = pj;
			} else {
				matriz[h][v] = pj;
			}

		} else if (instruccio.contains('d')) {
			matriz[h][v] = 0;

			v++;
			if (isOutOfBounds(h, v, matriz) || matriz[h][v] == 1 ||  matriz[h][v] == 9 || matriz[h][v] == 2 || matrizBombas[h][v] != 0) {
				v--;
				matriz[h][v] = pj;
			} else {

				matriz[h][v] = pj;
			}

		} else if (instruccio.contains('a')) {
			matriz[h][v] = 0;

			v--;
			if (isOutOfBounds(h, v, matriz) || matriz[h][v] == 1 ||  matriz[h][v] == 9 || matriz[h][v] == 2 || matrizBombas[h][v] != 0) {
				v++;
				matriz[h][v] = pj;
			} else {

				matriz[h][v] = pj;
			}

		} else if (instruccio.contains('s')) {
			matriz[h][v] = 0;

			h++;
			if (isOutOfBounds(h, v, matriz) || matriz[h][v] == 1 ||  matriz[h][v] == 9 || matriz[h][v] == 2 || matrizBombas[h][v] != 0) {
				h--;
				matriz[h][v] = pj;
			} else {

				matriz[h][v] = pj;
			}

		}
		Posicio p = new Posicio(h, v);
		return p;
	}
/**
 * Serveix per printar els grafics a la matriu
 * @param matriz
 * @param matrizBombas
 * @param matrizPowerUps
 * @param t
 * @param f
 */
	private static void printMatrizGrafica(int[][] matriz, int[][] matrizBombas, int[][] matrizPowerUps, Board t,
			Window f) {
		String[] imatges = { "", "Piedras_No.png", "Piedras_Si.png", "Carnage.gif", "Venom.gif", "ExplosionBomberman.png", "bomba.png", "Burger.png", "Pizza.png","Planta.png" };
		// 0 - nada, 1 - pared, 2- pared rompible, 3- personaje, 4- enemigo, 5-
		// explosion, 6- bomba, 7- power up 1, 8- power up 2
		t.setSprites(imatges);
		int[][][] cosa = new int[3][7][7];
		cosa[0] = matriz;
		cosa[1] = matrizBombas;
		cosa[2] = matrizPowerUps;
		t.draw(cosa);

	}
/**
 * Serveix per printar la matriu
 * @param matriz
 */
	private static void printMatriz(int[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}
/**
 * Serveix per mirar si et surts de la matriu
 * @param f
 * @param c
 * @param matriz
 * @return
 */
	public static boolean isOutOfBounds(int f, int c, int[][] matriz) {
		if (f < 0 || c < 0 || f >= matriz.length || c >= matriz[0].length) {
			return true;
		}
		return false;
	}
}
