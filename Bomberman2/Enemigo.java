package Bomberman2;

public class Enemigo {

	private Posicio p;
	private int hp;
	private boolean alive;

	@Override
	public String toString() {
		return "Enemigo [p=" + p + ", hp=" + hp + ", alive=" + alive + "]";
	}

	public Enemigo() {

	}

	public Enemigo(int f, int c, int hp) {
		this.p = new Posicio(f, c);
		this.hp = hp;
		this.alive = true;
	}

	public Posicio getPos() {
		return p;
	}

	public void setPos(Posicio p) {
		this.p = p;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

}
