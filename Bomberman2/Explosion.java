package Bomberman2;

public class Explosion {
	private Posicio p;
	private int ticks;

	public Explosion(int f, int c, int ticks) {
		this.p = new Posicio(f, c);
		this.ticks = ticks;
	}

	public Posicio getPos() {
		return p;
	}

	public void setPos(Posicio p) {
		this.p = p;
	}

	public int getTicks() {
		return ticks;
	}

	public void setTicks(int ticks) {
		this.ticks = ticks;
	}
}
